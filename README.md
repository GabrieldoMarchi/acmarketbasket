# ACMarketBasket

Projeto desenvolvido para a disciplina de projeto integrador da Fatec Shunji Nishimura em Pompeia-SP, que busca desenvolver uma aplicação web para a
 utilização de regras de associação em vendas.
 
 # Técnicas abordadas

  

- Utilização de regras de associação.
 
- Manipulação de dados.

- Aplicação web "Shiny APP"




### Ferramentas e Tecnologias utilizadas


 [R](https://www.r-project.org/)

 [Rstudio](https://rstudio.com/)

 [Shiny](https://shiny.rstudio.com/)

 [Pacote arulez](https://cran.r-project.org/web/packages/arules/)

 [Pacote Dplyr](https://dplyr.tidyverse.org/)

 [MS Office](https://www.office.com/)


### Desenvolvedores do projeto


Equipe formada por alunos do curso de Big Data no Agronegócio e seus perfis no LinkedIn.



| Equipe | Perfil no LinkedIn  |
| ------ | ------ |
| Gabriel Dias de Oliveira De Marchi | https://www.linkedin.com/in/gabriel-dias-de-oliveira-de-marchi-412230129/ |


Para mais informações do projeto: [wiki](https://gitlab.com/GabrieldoMarchi/acmarketbasket/-/wikis/home)

